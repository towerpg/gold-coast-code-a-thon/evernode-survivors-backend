const HotPocket = require('hotpocket-js-client');
const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const WebSocket = require('ws');

const app = express();
const server = http.createServer(app);

const wss = new WebSocket.Server({ server });

app.use(bodyParser.json());
let client;

const PORT = process.env.PORT || 3001;

wss.on('connection', function connection(ws) {
    console.log('WebSocket client connected');

    // Handle messages from clients
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
    });

    // Send a message to the client
    ws.send('Hello, Evernode Survivor!');
});

async function initializeClient() {
    const userKeyPair = await HotPocket.generateKeys();
    // client = await HotPocket.createClient(['wss://localhost:8081'], userKeyPair);
    client = await HotPocket.createClient(['wss://evernode.tequ.dev:26202'], userKeyPair);

    // Establish HotPocket connection.
    if (!await client.connect()) {
        console.log('Connection failed.');
        return null;
    }

    client.on(HotPocket.events.contractOutput, (result) => {
        console.log("Received outputs:");
        result.outputs.forEach((o) => {
            console.log(o);
            // Forward output to all WebSocket clients
            wss.clients.forEach((client) => {
                if (client.readyState === WebSocket.OPEN) {
                    client.send(JSON.stringify(o));
                }
            });
        });
    });

    console.log('HotPocket Connected.');
    return client;
}

async function submitContractInput(input) {
    if (!client) {
        console.log('Client not initialized.');
        return;
    }

    let meow = await client.getStatus();
    console.log(meow);
    await client.submitContractInput(input);
    console.log('Contract input submitted:', input);
}

async function clientApp() {
    const client = await initializeClient();
    if (!client) {
        console.log('Client initialization failed.');
        return;
    }
    // You can call submitContractInput multiple times with different inputs here.
}

app.post('/evernode', async (req, res) => {
    if (!client) {
        const initialized = await initializeClient();
        if (!initialized) {
            res.status(500).send('Failed to initialize client');
            return;
        }
    }

    const { userId, score } = req.body;
    if (!userId || !score) {
        res.status(400).send('userId and score are required');
        return;
    }
    const input = { userId, score };
    await submitContractInput(JSON.stringify(input));
    // await submitContractInput(input);
    console.log('Submitted contract input:', input);
    res.send(`Contract input submitted: ${JSON.stringify(input)}`);
});

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
    clientApp(); // Call clientApp once the server is running
});
